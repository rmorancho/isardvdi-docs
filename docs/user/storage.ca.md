# Emmagatzematge

Des de la vista principal de l'usuari, a **Emmagatzematge** es pot obtenir més d'informació sobre els discs dels escriptoris que s'han creat, així com el consum d'aquests i de plantilles.

![](storage.ca.images/storage1.png){width="80%"}

En aquest apartat podem veure:

- **ID**
- **Escriptoris/plantilles**: nom
- **Mida**: tamany que ocupa el disc de l'escriptori o plantilla en GB
- **% Quota**: percentatge de quota de disc ocupat
- **Últim accés**: indica l'últim cop que es va accedir a l'escriptori/es va crear la plantilla

!!! Info
    La **Mida** proporcionada en aquesta secció no té en compte la mida del disc total que es tria quan es crea l'escriptori. Els discs dels escriptoris comencen a ocupar emmagatzematge conforme es treballa amb ells i les dades comencen a ocupar espai.
