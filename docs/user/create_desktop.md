# Quickstart

There are two types of desktops:

* **Temporary**: When a desktop is shut down, **everything** that has been done, saved, or downloaded will be lost.
* **Persistent**: When it is turned off, the information or programs that have been installed will **not be lost**.

## Main view

To create a desktop, click the button ![](./create_desktop.images/create1.png){width="8%"} from the **Desktops** view.

![](./create_desktop.images/create2.png){width="80%"}

Fill in the fields, **select a template** and press the button ![](./create_desktop.images/create5.png){width="6%"}.

![](./create_desktop.images/create3.png){width="80%"}

You can change the desktop settings in the **"Advanced Options"** section:

!!! info inline end "Links"
        <font size="3">
            <ul>
            <li> [Viewers](../user/edit_desktop.md/#viewers)</li>
            <li> [RDP Login](../user/edit_desktop.md/#rdp-login)</li>
            <li> [Hardware](../user/edit_desktop.md/#hardware)</li>
            <li> [Reservables](../user/edit_desktop.md/#reservables)</li>
            <li> [Media](../user/edit_desktop.md/#media)</li>
            </ul>
        </font>

![](./create_desktop.images/viewers.png){width="60%"}
![](./create_desktop.images/rdp_login.png){width="60%"}
![](./create_desktop.images/hardware.png){width="60%"}
![](./create_desktop.ca.images/hardware_2.png){width="60%"}
![](./create_desktop.es.images/media.png){width="60%"}



