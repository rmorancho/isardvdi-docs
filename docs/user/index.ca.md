# Introducció

El manual d'usuari s'organitza seguint l'ordre del menú d'usuari i consisteix en:

- Els mètodes d'**Inici de sessió** disponibles per a accedir al sistema.
- **Escriptoris**, les seves accions i visors.
- **Plantilles** que permeten ser replicades com a múltiples escriptoris.
- Es poden pujar **Mitjans**, normalment imatges ISO per a crear escriptoris nous.
- **Desplegaments** que permeten supervisar els escriptoris mentre els usuaris hi treballen.
- Es necessiten **Reserves** per a recursos com les vGPU.
- **Emmagatzematge** mostra quant ocupen els discs d'escriptoris.
- **Administració** és un apartat que permet als gestors i administradors de la plataforma fer funcions avançades.
- **Perfil** mostra tant la informació d'usuari com la seva quota i ús.

![](index.images/global_view.png)