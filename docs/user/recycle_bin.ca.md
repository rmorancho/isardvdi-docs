# Paperera de reciclatge
Quan s'esborra un escriptori, una plantilla o un desplegament es pot escollir si es vol que aquest no desaparegui de manera permanent.  En lloc de ser eliminat de manera definitiva, l'element en qüestió es traslladarà a la paperera de reciclatge, on es podrà recuperar o eliminar definitivament, ja sigui de forma manual o automatitzada. Aquesta funció té l'objectiu de proporcionar una capa addicional de protecció per evitar l'eliminació accidental d'elements importants.

## Eliminació d'elements

!!! Danger "Molt important"
    - Un cop arribat al temps màxim a la paperera **l'element s'esborrarà automàticament de manera definitiva i no es podrà recuperar**.
    - L'emmagatzematge dels elements que es troben a la paperera de reciclatge **es considerarà a la quota d'usuari fins que s'esborrin de manera definitiva**.

### Eliminació d'un escriptori

**Per defecte**, en esborrar un escriptori aquest s'eliminarà de manera **definitiva**, però s'oferirà a l'usuari la possibilitat de enviar l'escriptori a la paperera de reciclatge mitjançant una casella de selecció. A més, s'indicarà a l'usuari el temps màxim que pot estar l'escriptori a la paperera de reciclatge.

![](./recycle_bin.ca.images/1.png)

### Eliminació d'una plantilla

En esborrar una plantilla aquesta anirà directament a la paperera de reciclatge, cal tenir en compte que **s'enviaran també totes les seves dependències**:

![](./recycle_bin.ca.images/2.png)

!!! Danger "Acció no reversible"
    En esborrar una plantilla definitivament **s'esborraran també totes les entrades depenents d'altres usuaris**. És a dir, si un usuari envia a la paperera de reciclatge un escriptori derivat d'una plantilla i posteriorment s'esborra definitivament la plantilla, s'esborrarà automàticament l'escriptori derivat de l'usuari de manera definitiva.

### Eliminació d'un desplegament

**Per defecte**, en esborrar un desplegament aquest s'eliminarà de **manera definitiva**, però s'oferirà a l'usuari la possibilitat de enviar el desplegament a la paperera de reciclatge. A més, s'indicarà a l'usuari el temps màxim que pot estar el desplegament a la paperera de reciclatge.

![](./recycle_bin.ca.images/3.png)

!!! Danger "Accions no reversibles"
    - En esborrar un escriptori concret d'un desplegament aquest **s'esborrara de manera definitiva i immediata**.
    - En modificar els usuaris d'un desplegament **s'esborraran de manera definitiva i immediata** tots els escriptoris dels usuaris que ja no formin part del desplegament.

## Gestió d'elements esborrats

Per gestionar els elements que es troben a la paperera de reciclatge, es prem la icona ![](./recycle_bin.ca.images/4.png) a la barra de navegació:

![](./recycle_bin.ca.images/5.png)

A l'apartat de paperera de reciclatge es podran visualitzar els diferents elements esborrats provisionalment.

![](./recycle_bin.ca.images/6.png)

### Veure informació dels elements esborrats

Es pot accedir fent clic a la fila corresponent o a la icona ![](./recycle_bin.ca.images/7.png). Es podrà accedir a la informació detallada dels elements esborrats i les seves dependències:

![](./recycle_bin.ca.images/8.png)

### Restaurar elements esborrats

Es poden recuperar els elements esborrats fent clic a la icona ![](./recycle_bin.ca.images/9.png).

![](./recycle_bin.ca.images/10.png)

### Eliminar definitivament

Es poden eliminar definitivament els elements esborrats fent clic a la icona ![](./recycle_bin.ca.images/11.png).

![](./recycle_bin.ca.images/12.png)

!!! Info "Recuperació d'espai"
    Un cop realitzada aquesta acció s'alliberarà l'espai corresponent de la quota d'usuari.