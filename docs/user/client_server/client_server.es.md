# Cliente-Servidor

En esta guía se explicará a modo manual de usuario los pasos necesarios para crear una conectividad hacia el escritorio de un alumno para poder supervisar el trabajo del aula en tiempo real, sobre el entorno de virtualización Isard VDI.

En el primer punto, se documenta cómo establecer una conexión entre el escritorio de un profesor y el de un alumno, formando así una estructura cliente-servidor. Ambos roles se conectarán mediante una misma red interna con la que el cliente pueda acceder al área del trabajo del servidor.

Se creará un entorno lo más real posible para ajustar la guía a la práctica. Se crearán unos usuarios y unos escritorios con los que trabajar. Si el entorno con escritorios, plantillas y usuarios ya está creado, se deben realizar únicamente los pasos que como administrador, profesor y alumno falten por realizar.


## Como usuario administrador

### Creación de usuarios y grupos

Para comenzar se crean unos usuarios de prueba con los que realizar la práctica. Lo más recomendado es que ya estén creados los grupos a los que vayan a pertenecer antes de crear los usuarios.

Para crear un grupo, se va al panel de Administración y en el apartado de "Users".

En el apartado de "Groups" se pulsa el botón ![](./client_server.images/client_server1.png)

![](./client_server.images/client_server2.png)

Y se rellena el formulario, en nuestro caso para el ejemplo hemos creado un grupo llamado "aula_100".

![](./client_server.images/client_server3.png)

El usuario Administrator crea un usuario profesor de rol Advanced, un aula de clase y cuatro alumnos rol Users con esta información:

![](./client_server.images/client_server4.png)

![](./client_server.images/client_server5.png)


### Interfaces de red

Para que los alumnos y el profesor estén conectados mediante una red interna y establezcan mediante esa red la conectividad, se conectan el escritorio del alumno y el del profesor con la misma interfaz de red.
Se puede usar una de las interfaces de red interna ya creadas que ofrece Isard.

El administrador se dirige al panel Administración y en el desplegable "Domains", se pulsa el botón "Resources". 

![](./client_server.images/client_server6.png)

En este apartado se muestra un listado de todas las interfaces de red ya creadas en el sistema. Existen las interfaces modo Network (que usará el escritorio del profesor) y modo OpenVSwitch (que usará el escritorio del profesor y el escritorio del alumno).

![](./client_server.images/client_server7.png)


### Compartir interfaz de red

Para que los usuarios puedan utilizar las redes, se tienen que compartir. La primera interfaz, "Default", ya está compartida por defecto con todos los usuarios del sistema, así que sólo se tiene que compartir la segunda interfaz, "Private 1".

Se pulsa el botón ![](./client_server.images/client_server8.png) y saldrá una ventana de diálogo con un formulario a rellenar.

Asignando el grupo "aula_100", se compartirá con todos los usuarios de ese mismo grupo:

![](./client_server.images/client_server9.png)


### Creación de escritorios

En este apartado se procede a crear dos plantillas respectivas para el profesor y el alumno que se compartirán también respectivamente. Para que así los usuarios puedan crear escritorios con los que trabajar en base a la plantilla modificada que se les ha preparado. Se pueden utilizar otras plantillas ya creadas en el entorno dado que la configuración de las redes irá en el escritorio; este paso se realiza en caso de que ni profesor ni alumno dispongan de plantillas.


#### Escritorio del profesor

Se crea la plantilla debian_profe desde por ejemplo un escritorio Debian 9.5.0, donde a su vez se comparte con el profesor test_docent01 y se asignan las interfaces de red Default y red interna:

![](./client_server.images/client_server10.png)


#### Escritorio de los alumnos

Finalmente se crea la plantilla ubuntu_alum desde por ejemplo un escritorio Ubuntu 20.04, la cual se comparte con el profesor test_docent01 y con el grupo aula_100 y se añade la interfaz Red interna:

![](./client_server.images/client_server11.png)


## Como usuario profesor

### Creación de escritorios

Mediante el login del profesor test_profe01, se crean dos escritorios en base a la plantilla compartida con los que se realizará la prueba de conexión. Un escritorio para el profesor y otro para los alumnos, creados con sus respectivas plantillas, que reciben el nombre de servidor_web (profesor) y cliente_web (alumno)

La creación de escritorios en base a plantilla se explican en el apartado de "User" en el manual.

**Los escritorios recién creados tendrán los mismos parámetros de hardware con los que se creó la plantilla; no se necesitará modificación dado que vienen preconfiguradas.**

![](./client_server.es.images/client_server1.png) 


## Conexión entre escritorios

Aunque hay muchas, aquí se explican dos formas de configurar las redes de los escritorios: una mediante interfaz gráfica y otra mediante línea de comandos o terminal.

Una vez creados se arrancan y se accede a ambos mediante los visores.

![](./client_server.images/client_server12.png) 


### Configuración del servidor

De forma automática en la primera interfaz de red de servidor_web se ofrece una dirección IP mediante el servicio DHCP de la red exterior, por lo que la máquina debe tener acceso a Internet por defecto.

Para configurar la segunda interfaz con red interna se le asigna una dirección IP de forma manual.

A partir de aquí:

#### Interfaz gráfica

En Parámetros, se pulsa el botón ![](./client_server.images/client_server13.png), y en el apartado IPv4 se configura la red de esta forma:

![](./client_server.ca.images/client_server14.png)

![](./client_server.ca.images/client_server15.png)

![](./client_server.ca.images/client_server16.png)

![](./client_server.ca.images/client_server17.png)

Se puede comprobar fácilmente abriendo una terminal y escribiendo el comando 

``` 
ip -c a 
```

Devuelve una salida parecida a esta:

![](./client_server.images/client_server18.png)


#### Línea de comandos

Para hacerlo con la terminal se usa el paquete network-manager que debe instalarse en la máquina, y mediante el software nmcli y los comandos siguientes se configura la segunda interfaz, donde el valor nombre_interfaz corresponde a la tarjeta de red que queremos modificar.

Si el nombre no es claro, con el siguiente comando se puede comprobar:

```
sudo nmcli connection show
```

![](./client_server.images/client_server19.png)

```
sudo nmcli connection down nombre_interfaz
sudo nmcli connection modify nombre_interfaz ipv4.method manual ipv4.address 192.168.200.10/24
sudo nmcli connection up nombre_interfaz
```

Con el mismo comando comprobamos la modificación de la interfaz:

```
ip -c a
```

![](./client_server.images/client_server20.png)


### Configuración del cliente

Se le asigna a continuación una dirección IP a cliente_web para la red interna, de esta forma para Ubuntu:

#### Interfaz gráfica

En el apartado Configuración de red cableada:

![](./client_server.es.images/client_server21.png)

![](./client_server.es.images/client_server22.png)

![](./client_server.es.images/client_server23.png)

Con el mismo comando se hace la comprobación:

![](./client_server.images/client_server24.png)

#### Línea de comandos

El valor nombre_interfaz corresponde a la tarjeta de red que queremos modificar.

Si el nombre no es claro, con el siguiente comando se puede comprobar:

```
sudo nmcli connection show
```
![](./client_server.images/client_server25.png)

Se abre un terminal y mediante el paquete "network-manager" se configura la interfaz:

```
sudo nmcli connection down nombre_interfaz
sudo nmcli connection modify nombre_interfaz ipv4.method manual ipv4.addresses 192.168.200.11/24
sudo nmcli connection up nombre_interfaz
```

Con el mismo comando comprobamos la modificación de la interfaz:

```
ip -c a
```

![](./client_server.images/client_server26.png)


### Prueba de conexión

#### Comando ‘ping’

Para comprobar la correcta comunicación entre escritorios, con el comando ping se envía paquetería hacia la dirección IP o dominio descrita para comprobar una conexión entre ambas máquinas. Se realiza el comando en ambas máquinas y devuelve así una respuesta como esta, comprobando que la conexión existe y se comunican:

![](./client_server.images/client_server27.png)

#### Entorno web con ‘Apache’

Para terminar, se puede probar también mediante el servidor de HTTP Apache que se establece conexión.
Se instala mediante terminal en el escritorio servidor_web y se conecta cliente_web al software mediante el navegador web de la máquina.

![](./client_server.ca.images/client_server28.png)

Se comprueba la instalación de Apache y se visualiza la existencia del fichero HTML de comprobación que muestra el servicio cuando te conectas a él:

![](./client_server.ca.images/client_server29.png)

El cliente_web solo tiene que abrir un navegador web y escribir la dirección IP de la máquina del servidor para que Apache responda:

![](./client_server.images/client_server30.png)


