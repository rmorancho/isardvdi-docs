# SAML Access

Press the button ![](saml_access.images/button.png){width="5%"} from the main IsardVDI page.

![](local_access.images/local_access1.png){width="80%"}

From now, continue through the login process known by the user, which is different between each SAML implementation. 


## Registration by enrollment key

Enter the code provided by the administrator or the manager of the platform and press the button ![](oauth_access.ca.images/codi2.png){width="30%"}

![](./oauth_access.images/code1.png){width="30%"}

And you access the main page.

![](./oauth_access.images/home1.png){width="80%"}

!!! note
    Once authenticated, there is **no** need to repeat the registration process. When you want to access again through SAML, just click on the button ![](./saml_access.images/button.png){width="5%"} and use the **SAML credentials**.




