# Acceso mediante OAuth

Se pulsa el botón ![](oauth_access.ca.images/codi3.png){width="8%"} de la pantalla principal de IsardVDI.

![](local_access.es.images/local_access1.png){width="80%"}

Pedirá una cuenta de correo de Google

![](./oauth_access.es.images/google1.png){width="40%"}
![](./oauth_access.es.images/google2.png){width="40%"}


## Inscripción por código de registro

Se introduce el [codigo de autoregistro proporcionado](../manager/manage_user.es.md/#clave-de-autoregistro) por un administrador o gestor de la plataforma y se pulsa el botón ![](./oauth_access.es.images/codigo2.png){width="30%"}

![](./oauth_access.es.images/codigo1.png){width="30%"}

Y se accede a la página principal.

![](./oauth_access.es.images/home1.png){width="80%"}

!!! note "Nota"
    Una vez autenticado el usuario, ya **no** hace falta repetir el proceso de registro. Al próximo inicio de sesión, sólo se tiene que pulsar en el botón ![](oauth_access.ca.images/codi3.png){width="8%"} y utilizar las **credenciales de Google**.
