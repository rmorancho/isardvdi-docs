# Accés per visor directe

L'usuari **advanced** o **manager** pot [generar un enllaç](../../advanced/direct_viewer.ca/) perquè un usuari es pugui connectar al visor d'un escriptori sense necessitat d'estar autenticat a la plataforma.

D'aquesta manera, només cal compartir l'enllaç del visor directe i escriure'l al navegador web. Això permetrà seleccionar un visor, que hagi estat previament triat, per accedir a l'escriptori.

![](direct_viewer_access.ca.images/visor1.png)