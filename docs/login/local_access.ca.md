# Accés local

Havent adquirit una **categoria disponible** a la plataforma, administrada pels administradors o gestors del sistema, es tria l'**idioma** i s'escriuen les **credencials de l'usuari** (es pot seleccionar la **categoria al desplegable** sota l'idioma o escrivint a l'enllaç d'accés **https://domini_isardvdi/login/nom_categoria**):

![](./local_access.ca.images/local_access1.png){width="80%"}

!!! note "Nota"
    L'elecció de l'**idioma** es queda guardada a les **galetes del navegador**, així al pròxim inici de sessió de l'usuari, aquest ja apareixerà **preseleccionat**.