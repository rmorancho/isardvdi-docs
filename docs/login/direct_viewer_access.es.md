# Acceso por Visor Directo

El usuario **advanced** o **manager** puede [generar un enlace](../advanced/direct_viewer.es.md) para que un usuario se pueda conectar al visor de un escritorio sin necesidad de estar autenticado en la plataforma.

De esta manera, solo se tiene que compartir el enlace del visor directo y escribirlo en el navegador web. Esto permitirá seleccionar un visor, previamente seleccionado, para acceder al escritorio.

![](./direct_viewer_access.es.images/visor1.png)