# Access via OAuth

Press the button ![](oauth_access.ca.images/codi3.png){width="8%"} from the main page of IsardVDI.

![](./local_access.images/local_access1.png){width="80%"}

It will ask for a Google email account

![](./oauth_access.images/google1.png){width="40%"}
![](./oauth_access.images/google2.png){width="40%"}


## Registration by enrollment key

Enter the provided [self-registration code](../manager/manage_user.md/#enrollment-keys) provided by an administrator or platform manager, and press the button ![](./oauth_access.images/code2.png){width="30%"}

![](./oauth_access.images/code1.png){width="30%"}

And you access the main page.

![](./oauth_access.images/home1.png){width="80%"}

!!! note
    Once the user is authenticated, there is **no** need to repeat the registration process. When you want to login again, just click on the button ![](oauth_access.ca.images/codi3.png){width="8%"} and use the **Google credentials**.

