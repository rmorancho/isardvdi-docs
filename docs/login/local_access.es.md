# Acceso Local

Si se ha adquirido una **categoría disponible** a la plataforma, administrada por los administradores o gestores del sistema, se selecciona el **idioma** y se introducen las **credenciales del usuario** (se puede elegir la **categoría en el menú desplegable** debajo del idioma o escribiendo en el enlace de acceso **https://dominio_isardvdi/login/nombre_categoria**):

![](./local_access.es.images/local_access1.png){width="80%"}

!!! note "Nota"
    La elección del **idioma** queda guardada en las **cookies del navegador**, por lo que en el próximo inicio de sesión del usuario, este ya estará **preseleccionado**.

