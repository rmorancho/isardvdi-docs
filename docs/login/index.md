# Introduction

To access the server you have to go to **https://isardvdi_domain** where the login page will be displayed.

![](./local_access.images/local_access1.png){width="80%"}

!!! info "The user account could be one of this types:"
        - **[Local](../login/local_access.md)**: The **category** and **credentials** have been provided.
        - **[OAuth2 (Google/Gitlab)](../login/oauth_access.md)**: A necessary **access code** has been provided to complete registration with a personal account.
        - **[SAML](../login/saml_access.md)**: Corporate credentials to log in. An **access code** has also been provided to complete self-registration.


