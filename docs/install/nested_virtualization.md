# Nested installation in KVM

Check for nested virtualization option in your host operating system:

- Intel processors: ```cat /sys/module/kvm_intel/parameters/nested```
- AMD processors: ```cat /sys/module/kvm_amd/parameters/nested```

It should show a **1** or **Y** if it is enabled.

You will need to enable nested virtualization on your host operating system if not active yet.

## Nested virt in Intel processors:

### Live

With all VMs stopped remove kvm_intel module
```
modprobe -r kvm_intel
```
And load it again with nested option:
```
modprobe kvm_intel nested=1
```

### Permanent

Create the file ```/etc/modprobe.d/kvm.conf``` and add inside:
```
options kvm_intel nested=1
```

## Nested virt in AMD processors:

### Live

With all VMs stopped remove kvm_amd module
```
modprobe -r kvm_amd
```
And load it again with nested option:
```
modprobe kvm_amd nested=1
```

### Permanent

Create the file ```/etc/modprobe.d/kvm.conf``` and add inside:
```
options kvm_amd nested=1
```