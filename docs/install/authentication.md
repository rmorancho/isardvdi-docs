# Authentication

IsardVDI supports different authentication providers. There can be multiple providers in the same installation

## OAuth2

You should set up your application auth tokens to enable this kind of logins.

- **BACKEND_HOST**: Set it to your domain
- **BACKEND_AUTH_AUTOREGISTRATION**: Activate auto registering

### Google

- **BACKEND_AUTH_GOOGLE_ID**: Set your google ID.
- **BACKEND_AUTH_GOOGLE_SECRET**: Set your google secret.

### Github

- **BACKEND_AUTH_GITHUB_ID**: Set your github ID.
- **BACKEND_AUTH_GITHUB_SECRET**:  Set your github secret.

## LDAP

The LDAP authentication uses the same form that the local login.

In order to configure the LDAP authentication, we have to set the following parameters: [isardvdi.cfg.example](https://gitlab.com/isard/isardvdi/-/blob/main/isardvdi.cfg.example#L75-L103)

We'll go through each configuration parameter:

| Parameter | Default value | Description |
| --------- | ------------- | ----------- |
| `AUTHENTICATION_AUTHENTICATION_LDAP_ENABLED` | `false` | If set to `true`, this will enable the LDAP authentication  |
| `AUTHENTICATION_AUTHENTICATION_LDAP_PROTOCOL` | `ldap` | The LDAP protocol. Other possible values are `ldaps`|
| `AUTHENTICATION_AUTHENTICATION_LDAP_HOST` | | The LDAP server |
| `AUTHENTICATION_AUTHENTICATION_LDAP_PORT` | 389 | The LDAP port where the server is listening |
| `AUTHENTICATION_AUTHENTICATION_LDAP_BIND_DN` | | The DN that Isard is going to use to query the LDAP |
| `AUTHENTICATION_AUTHENTICATION_LDAP_PASSWORD` | | The password that Isard is going to use to query the LDAP |
| `AUTHENTICATION_AUTHENTICATION_LDAP_BASE_SEARCH` |  | The DN that all the users share (e.g. `ou=people,dc=example,dc=com`) |
| `AUTHENTICATION_AUTHENTICATION_LDAP_FILTER` | `(&(objectClass=person)(uid=%s))` | The filter that Isard is going to use to find each user. The `%s` represents the username that gets sent through the form. More information [here](https://confluence.atlassian.com/kb/how-to-write-ldap-search-filters-792496933.html) |
| `AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_UID` |  | The field from the LDAP entry that contains the user UID |
| `AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_UID` | `.*` | The regex filter that is going to be applied in the field. By default it gets the whole field |
| `AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_USERNAME` |  | The field from the LDAP entry that contains the user username |
| `AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_USERNAME` | `.*` | The regex filter that is going to be applied in the field. By default it gets the whole field |
| `AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_NAME` |  | The field from the LDAP entry that contains the user name |
| `AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_NAME` | `.*` | The regex filter that is going to be applied in the field. By default it gets the whole field |
| `AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_EMAIL` |  | The field from the LDAP entry that contains the user email |
| `AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_EMAIL` | `.*` | The regex filter that is going to be applied in the field. By default it gets the whole field |
| `AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_PHOTO` |  | The field from the LDAP entry that contains the user photo |
| `AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_PHOTO` | `.*` | The regex filter that is going to be applied in the field. By default it gets the whole field |

In regards of the field / regex configuration parameters: these are the fields that the LDAP search responds. For example, in some installations, the field for the email is called 'mail'. Then, a regex is applied to this field, in case we needed to filter inside a LDAP field. By default it collects the whole field. The regex match tries to extract the first group, but if there's no group it will extract the whole match

With this, the LDAP authentication is going to work. However, there's a ✨ *extra feature* ✨! With the LDAP authentication we can autoregister the users in groups, so there's no need to use the registration codes.

### Autoregistration

If a group has a registration code, and the LDAP auto registration is enabled, the LDAP autoregistration is always going to take preference. To configure it, we have to set the following configuration parameters: [isardvdi.cfg.example](https://gitlab.com/isard/isardvdi/-/blob/main/isardvdi.cfg.example#L105-L131)

We'll go through each parameter:


| Parameter | Default value | Description |
| --------- | ------------- | ----------- |
| `AUTHENTICATION_AUTHENTICATION_LDAP_AUTO_REGISTER` | `false` |  If set to `true`, this will enable the LDAP auto registration |
| `AUTHENTICATION_AUTHENTICATION_LDAP_GUESS_CATEGORY` | `false` | If set to `true`, Isard is going to attempt to guess the category based in the search results, instead on relying in the category ID provided by the form. This enables multiple categories to use the same form and login URL  |
| `AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_CATEGORY` | | The field from the LDAP entry that contains the user category |
| `AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_CATEGORY` | `.*` | The regex filter that is going to be applied in the field. By default it gets the whole field |
| `AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_GROUP` | | The field from the LDAP entry that contains the user group |
| `AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_GROUP` | `.*` | The regex filter that is going to be applied in the field. By default it gets the whole field |
| `AUTHENTICATION_AUTHENTICATION_LDAP_GROUPS_SEARCH` | | The DN that all the groups share (e.g. `dc=example,dc=com`) |
| `AUTHENTICATION_AUTHENTICATION_LDAP_GROUPS_FILTER` | `(&(objectClass=posixGroup)(memberUid=%s))` | The filter that Isard is going to use to find each user. The `%s` represents the username that gets sent through the form. More information [here](https://confluence.atlassian.com/kb/how-to-write-ldap-search-filters-792496933.html) |
| `AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_ADMIN_GROUPS` | | A comma separated list of the groups that are going to be part of the `admin` role |
| `AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_MANAGER_GROUPS` | | A comma separated list of the groups that are going to be part of the `manager` role |
| `AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_ADVANCED_GROUPS` | | A comma separated list of the groups that are going to be part of the `advanced` role |
| `AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_USER_GROUPS` | | A comma separated list of the groups that are going to be part of the `user` role |
| `AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_DEFAULT` | `user` | The default role that the user is going to be assigned if it doesn't match with a group - role mapping. Possible values are `admin`, `manager`, `advanced` and `user` |

In regards of the field / regex configuration parameters: these are the fields that the LDAP search responds. For example, in some installations, the field for the email is called 'mail'. Then, a regex is applied to this field, in case we needed to filter inside a LDAP field. By default it collects the whole field. The regex match tries to extract the first group, but if there's no group it will extract the whole match

### Example LDAP configurations

LDAP with autoregistration
```
## LDAP
AUTHENTICATION_AUTHENTICATION_LDAP_ENABLED=true
AUTHENTICATION_AUTHENTICATION_LDAP_PROTOCOL=ldap
AUTHENTICATION_AUTHENTICATION_LDAP_HOST=ldap.example.com
#AUTHENTICATION_AUTHENTICATION_LDAP_PORT=389

### Credentials used for querying the LDAP
AUTHENTICATION_AUTHENTICATION_LDAP_BIND_DN="uid=isard,ou=users,dc=example,dc=com"
AUTHENTICATION_AUTHENTICATION_LDAP_PASSWORD=password

### Base Search is the DN that all the users share, e.g. ou=people,dc=example,dc=com
AUTHENTICATION_AUTHENTICATION_LDAP_BASE_SEARCH=dc=example,dc=com
### Filter is the actual filter used to search users. The '%s' represents the user that is sent through the form
### More information: https://confluence.atlassian.com/kb/how-to-write-ldap-search-filters-792496933.html
AUTHENTICATION_AUTHENTICATION_LDAP_FILTER="(&(objectClass=person)(uid=%s))"

### These are the fields that the LDAP search responds. For example, in some installations, the field for the email is called 'mail'
### Then, a regex is applied to this field, in case we needed to filter inside a LDAP field. By default it collects the whole field
AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_UID=uid
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_UID=.*
AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_USERNAME=uid
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_USERNAME=.*
AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_NAME=sn
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_NAME=.*
AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_EMAIL=mail
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_EMAIL=.*
#AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_PHOTO=
#AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_PHOTO=.*

### Auto Register the existing ldap users into IsardVDI
AUTHENTICATION_AUTHENTICATION_LDAP_AUTO_REGISTER=true
AUTHENTICATION_AUTHENTICATION_LDAP_GUESS_CATEGORY=true
### These are the fields that the LDAP search responds. For example, in some installations, the field for the group is called 'group'
### Then, a regex is applied to this field, in case we needed to filter inside a LDAP field. By default it collects the whole field
AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_CATEGORY=homeDirectory
AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_CATEGORY="/home/users/([^/]+)/[^/]+/[^/]+"
AUTHENTICATION_AUTHENTICATION_LDAP_FIELD_GROUP=homeDirectory
AUTHENTICATION_AUTHENTICATION_LDAP_REGEX_GROUP="/home/users/[^/]+/([^/]+)/[^/]+"
### The base search for listing all the groups of a user
AUTHENTICATION_AUTHENTICATION_LDAP_GROUPS_SEARCH=dc=example,dc=com
### Filter is the actual filter used to search all the groups of a user. The '%s' represents the user that is sent through the form
### More information: https://confluence.atlassian.com/kb/how-to-write-ldap-search-filters-792496933.html
AUTHENTICATION_AUTHENTICATION_LDAP_GROUPS_FILTER="(&(objectClass=posixGroup)(memberUid=%s))"
### The field that contains the group in the AUTHENTICATION_AUTHENTICATION_LDAP_GROUPS_FILTER search
AUTHENTICATION_AUTHENTICATION_LDAP_GROUPS_SEARCH_FIELD=cn
#AUTHENTICATION_AUTHENTICATION_LDAP_GROUPS_SEARCH_REGEX=.*
### All the users that are in at least one of the groups specified here, will be created in the admin role (comma separated)
AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_ADMIN_GROUPS=isard-admins
AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_MANAGER_GROUPS=isard-managers
AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_ADVANCED_GROUPS=profes,ofi
#AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_USER_GROUPS=ldapusers
# This is the default role that users will have if they don't match in any of the previous groups.
# Values can be 'admin', 'manager', 'advanced', 'user'
AUTHENTICATION_AUTHENTICATION_LDAP_ROLE_DEFAULT=user
```

## SAML

    Still not documented, refer to isardvdi.cfg