# Config

!!! info "Role access"

    Only **administrators** have access to this feature.

To go to the "Config" section, press the button ![](./users.images/users1.png)

![](./users.images/users2.png)

And press the button ![](./config.images/config1.png)

![](./config.images/config2.png)


## Job Scheduler

En este apartado se puede programar un "trabajo", puede ser una copia de seguridad o 

* Type: Cron (que haga el trabajo a una hora determinada), interval (que haga el trabajo cada x tiempo)

* Hour: indica la hora

* Minute: indica el minuto

* Action: backup database, check ephimeral domain status

## Maintenance mode

Maintenance mode disables web interfaces for users with manager, advanced, and user roles.

System administrator can create a file named `/opt/isard/config/maintenance` to switch to maintenance mode at start up. The file is removed once system is switched to maintenance mode.

Maintenance mode can be enabled and disabled via [configuration](./config.md) section of administration interface by an user with administrator role.

To enable and disable this function, check the box

![](./config.images/config6.png)

Once the box is checked, when users try to log in to their account, they will see a maintenance message

![](./config.images/config7.png)

![](./config.images/config8-es.png)

