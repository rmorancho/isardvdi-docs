# Introducció

!!! info "Rols amb accés"

    Només els **gestors** (managers) i els **administradors** tenen accés a aquesta característica.
    Els gestors estan restringits a la seva pròpia categoria.

Aquesta guia per administradors està organitzada com el menú del web. Els *rols gestors* (managers) tenen opcions restringides i accés només a dades de la seva pròpia categoria:

![](domains.images/global_view.png)

Els rols administradors tenen accés a més opcions al menú:

![](hypervisors.images/three_hypers.png)

