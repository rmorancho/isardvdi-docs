# Authentication

To manage the authentication policies you have to go to the Administration panel, press the button ![](./authentication.images/authentication1.png)

![](./authentication.images/authentication2.png)

Press the button ![](./authentication.images/authentication3.png)

![](./authentication.images/authentication4.png)

## Authentication policies

Different password policies can be applied in order to enhance the security of the platform.

### Create

Look for the "Users policy" table and press the button ![](./authentication.images/authentication5.png)

![](./authentication.images/authentication6.png)

And a dialog box will appear with the following form:

![](./authentication.images/authentication7.png)

Where it can be selected which category and role will the policy be applied to and the different restrictions applied to the users passwords:

- Digits: Minimum number of numerical digits.
- Character length: Minimum character length of the password.
- Lowercase chars: Minimum number of lowercase characters.
- Uppercase chars: Minimum number of uppercase characters.
- Special chars: Minimum number of special characters.
- No username: Whether the password can contain the username.
- Expiration days: Number of days until the password expires. For more information please refer to [password expiration](authentication.md#password-expiration) section.
- Old passwords: Number of old passwords to be remembered (current password can't be same as any of the last N passwords).

It can also be defined whether the email must be verified in order to use the platform. For more information please refer to [email validation](authentication.md#email-validation) section.


### Edit

Policies can be edited by pressing the icon ![](./authentication.images/authentication8.png) next to the policy we want to update.

A dialog box will pop up with the same parameters as the creation one.

![](./authentication.images/authentication9.png)

!!! Info "Important notes about the form fields"
    * The policy **category** and **role** fields can't be modified.

### Delete

To delete a policy, press the button ![](./authentication.images/authentication10.png) in the policies list.


### Force reset

To force the reset, press the button ![](./authentication.images/authentication17.png) in the policies list. And a dialog box will appear with the following options

![](./authentication.images/authentication18.png)

- When clicking "Force Update Password" all the users with the policy applied will forced to update their password. For more information please refer to [password expiration](authentication.md#password-expiration) section.
- When clicking "Force Verify Email Password" all the users with the policy applied will forced to verify their email. For more information please refer to [email validation](authentication.md#email-validation) section.

### Email validation

When creating a policy with email verification, users will be required to enter an email address and validate it when accessing the platform (excluding users logged through [external authentication providers](../../docs/install/authentication.md)).

!!! Danger "Be careful"
    When enabling it, users won't be able to access the platform until an email is validated.

Once a user has logged in, the following screen will be shown

![](./authentication.images/authentication11.png)

The user must write its email address and click on the "Send email" button. An alert will inform the user that a mail has been correctly sent to the given email address.

![](./authentication.images/authentication12.png)

Then the following email will be sent to the given email for verification.

![](./authentication.images/authentication13.png)

When clicking the "Verify email" button the user will be redirected to a page showing that the email has been succesfully validated.

![](./authentication.images/authentication14.png)

Since the email has been validated, the next time the user logs in it will be able to access the platform.

### Password expiration

When creting a policy expiration days, users will be required to update the password when accessing the platform (excluding users logged through [external authentication providers](../../docs/install/authentication.md)).

!!! Danger "Be careful"
    When enabling it, users won't be able to access the platform until its password is updated.

Once a user has logged in, the following screen will be shown. The blue alert box will show the password requirements for the user.

![](./authentication.images/authentication15.png)

When clicking the "Update" button the users password will be updated and a green alert box will be shown, redirecting the user to the login page.

![](./authentication.images/authentication16.png)

Since the password has been updated, the next time the user logs in it will be able to access the platform.