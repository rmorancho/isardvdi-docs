# Plantillas

Una plantilla es un escritorio preconfigurado. Su disco no se puede modificar, de forma que no se pueden iniciar como escritorios, pero sus parámetros se pueden personalizar para satisfacer las necesidades de cada usuario.

Las plantillas están diseñadas para ser compartidas con otros usuarios para que puedan crear sus propios escritorios.

Este es un ejemplo que ilustra la relación de las plantillas y los discos:

**1.** Se crea un escritorio con el disco de almacenamiento **D1**.

``` mermaid
graph LR
 dt1(Escritorio):::dt -.- dk1([D1]):::dk
 classDef dk fill:#ffd1dc,stroke:#ff3465,stroke-width:1px
 classDef dt stroke-width:2px
```

**2.** A continuación, se crea una plantilla a partir de este escritorio. Al crear la plantilla, el disco **D1** se asociará con la plantilla nueva. Al mismo tiempo, se hace una copia de **D1** y se denomina **D1'**. Este disco copiado será utilizado por el escritorio original en el futuro, de forma que cualquier cambio hecho al disco original no le afectará.

``` mermaid
graph LR
 dt1(Escritorio):::dt -.- dk1([D1']):::dk;
 dt1 --> tp1(Plantilla):::tp
 tp1 -.- dk2([D1]):::dk
 classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
 classDef dt stroke-width:2px
 classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**3.** Si se crea un escritorio nuevo a partir de esta plantilla, se crea un disco nuevo **D2** para el escritorio nuevo. Este disco **D2** contiene los cambios que se harán al escritorio respecto al disco de la plantilla **D1**. En otras palabras, **D2** está enlazado a **D1** y el escritorio obtendrá su información de **D1** en el momento de iniciarlo.

En decir, **D2** solo contiene los cambios hechos a **D1** que son relevantes para el escritorio nuevo, y los dos discos permanecen conectados entre sí.

``` mermaid
graph LR
 dt1(Escritorio):::dt -.- dk1([D1']):::dk;
 dt1 --> tp1(Plantilla):::tp
 tp1 -.- dk2([D1]):::dk
 tp1 --> dt2(Escritorio):::dt
 dt2 -.- dk3([D2]):::dk
 dk3 -- depende de --> dk2
 classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
 classDef dt stroke-width:2px
 classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

**4.** Al duplicar una plantilla, no se creará ningún disco nuevo. En lugar de esto, la nueva plantilla usará el mismo disco **D1** que la plantilla original.

``` mermaid
graph LR
 dt1(Escritorio):::dt -.- dk1([D1']):::dk;
 dt1 --> tp1(Plantilla):::tp
 tp1 -.- dk2([D1]):::dk
 tp1 --> tp2(Plantilla):::tp
 tp2 -.- dk2
 classDef dk stroke:#ff3465,fill:#ffd1dc,stroke-width:1px
 classDef dt stroke-width:2px
 classDef tp stroke:#30d200,fill:#cdffbe,stroke-width:2px

```

Entender la relación entre plantillas, escritorios y discos es importante para poder gestionar mejor la infraestructura de escritorios virtual.

## Crear 

Para crear una plantilla a partir de un escritorio se pulsa el icono siguiente:

![](./templates.es.images/templates2.png){width="80%"}

Se le puede asignar cualquier nombre/descripción, escoger si se quiere habilitar/deshabilitar (hacerla visible/invisible), y compartirla con grupos/usuarios

![](./templates.es.images/templates5.png){width="80%"}


## Tus Plantillas

Para ver las plantillas que has creado, se tiene que dirigir al apartado de tus plantillas personales.

![](./templates.es.images/templates8.png){width="80%"}

### Editar

Para editar una plantilla, al apartado **Tus plantillas** se pulsa el icono ![](./templates.es.images/templates10.png){width="3%"}, donde redirige a la página donde poder editar la información de la plantilla (mismo fomulario y opciones como cuando se **[edita un escritorio](../user/edit_desktop.es.md)**)

### Compartir

Para compartir una plantilla, al apartado **Tus plantillas** se pulsa el icono ![](./templates.es.images/templates13.png){width="3%"} y saldrá una ventana donde se podrán asignar los grupos y/o usuarios.

![](./templates.es.images/templates11.png){width="60%"}


### Hacer visible/invisible

Para modificar la visibilidad de una plantilla, en el apartado **Tus plantillas**, se hace click en el botón ![](./templates.ca.images/templates15.png){width="3%"} o el botón ![](./templates.ca.images/templates16.png){width="3%"} dependiendo de la visibilidad actual de ésta.

**El estado del ojo determinará la visibilidad de la plantilla**:

- Ojo abierto y botón azul ![](./templates.ca.images/templates15.png){width="3%"}: plantilla visible
- Ojo negado y botón gris ![](./templates.ca.images/templates16.png){width="3%"}: plantilla invisible

Aparecerá un formulario donde aceptar o negar el cambio de la visibilidad de la plantilla:

![](./templates.es.images/templates17.png){width="25%"}
![](./templates.es.images/templates19.png){width="25%"}


### Eliminar

!!! Danger "Eliminar plantillas"
    Al crear una plantilla desde un escritorio, se duplica su disco. Al generar un escritorio nuevo a partir de una plantilla, se crea un disco adicional en el almacenamiento que depende del disco de la plantilla original. Los **usuarios advanced solamente pueden borrar sus propias plantillas**, si hay **dependecias** con otros usuarios, se tendrá que informar al usuario manager/administrador.

Para borrar una plantilla se pulsa el icono ![](./media.es.images/media12.png){width="4%"}

![](./templates.es.images/template20.png){width="80%"}

La gestión de plantillas en el sistema puede variar según la presencia de dependencias. Dependiendo de si la plantilla tiene o no dependencias, se generará una notificación específica.

**Para plantillas sin dependencias:**

Cuando una plantilla no tiene dependencias, lo que significa que no se han creado escritorios de otros usuarios basados en esa plantilla, es posible eliminarla sin problemas. En este caso, se puede proceder con la eliminación de la plantilla de manera directa.

![](./templates.es.images/template21.png){width="80%"}

**Para plantillas con dependencias:**

Si una plantilla tiene dependencias, lo que indica que se han creado escritorios de otros usuarios que se originan a partir de esa plantilla, se requiere una acción diferente. En este caso, es necesario notificar al manager o administrador correspondiente sobre la intención de eliminar la plantilla. El manager o administrador tomará las medidas adecuadas para evaluar la situación y, en su caso, proceder con la eliminación de la plantilla, garantizando que los escritorios de otros usuarios se vean afectados o no en el proceso.

Siendo usuario **advanced**, si tiene dependencias, lo **indicará con un  "--"**, que se muestra cuando no se cuenta con los permisos necesarios para acceder a la información. Siendo usuario **manager**, indicará nombres de usuarios y escritorios de tu propia categoría, mientras que siendo usuario **administrador**, se tendrá acceso a información de todas las categorías.

![](./templates.es.images/template22.png){width="80%"}


## Compartidas contigo

En este apartado se pueden ver las plantillas que han sido compartidas con tu usuario.

![](./templates.es.images/templates9.png){width="80%"}

### Duplicar plantilla

Para duplicar una plantilla y hacerla tuya, al apartado **Plantillas compartidas contigo**, se selecciona la plantilla pulsando el icono![](./templates.images/templates14.png){width="3%"}

Se redirige a la página donde poder duplicarla.

![](./templates.es.images/templates6.png){width="80%"}

!!! Info "Importante"
        La duplicación de una plantilla compartida crea una **copia** donde el usuario es el propietario. Esto le permite **personalizar** la plantilla, incluyendo la modificación de los usuarios con los cuales se comparte.

