# Documentació API

Actualment estem avaluant *swagger* per a la documentació.

Mentrestant podeu comprovar els extrems (*endpoints*) al codi [api views](https://gitlab.com/isard/isardvdi/-/tree/main/api/src/api/views) i els paràmetres per a POST/PUT als esquemes [cerberus](https://gitlab.com/isard/isardvdi/-/tree/main/api/src/api/schemas).

Teniu un exemple d'accés a la API a [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/scripts/test_one_endpoint.py](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/scripts/test_one_endpoint.py)