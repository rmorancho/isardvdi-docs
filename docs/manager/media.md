# Media

Press the button ![](./media.images/media1.png)

![](./media.images/media2.png)

## User

Press the button ![](./media.images/media3.png) under the "User" section

![](./media.images/media4.png)

Here you can see two types of "Media":

1. **Media files**: In this section you can see the "Media" created by the user.

1. **Media shared with you**: In this section you can see the "Media" shared with the user.

![](./media.images/media5.png)

## Manager

Press the button ![](./media.images/media3.png) under the "Manager" section

![](./media.images/media6.png)

Here you can see all the "Media" shared with the same category as the user.

![](./media.images/media7.png)