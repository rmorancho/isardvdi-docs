# Administration

!!! info "Role access"

    Only **managers** and **administrators** have access to this feature.
    Managers are restricted to it's own category.

The administration web interface allows managers and administrators to setup and manage advanced features and manage all system users, desktops, templates and media.

Administration role users have also access to IsardVDI downloads, hypervisor management and system configuration.

In the [**Administration Guide**](../admin/) you'll find all this features.