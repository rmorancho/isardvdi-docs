# Introducció

## Rols

IsardVDI té quatre tipus diferents de rols d'usuari, cadascun hereta les capacitats de rols més bàsics. Aquesta és la jerarquia des de l'usuari bàsic a l'administrador:

- **usuari**: És l'usuari bàsic, generalment assignat als estudiants a les escoles. Aquest rol pot crear escriptoris a partir de plantilles compartides amb ell.
- **avançat**: Aquest rol s'assigna habitualment als professors de les escoles. Permet a l'usuari avançat crear i compartir plantilles dels seus escriptoris amb altres usuaris. També té la capacitat de crear desplegaments d'escriptori a altres usuaris.
- **gestor**: Té les capacitats per accedir a la interfície d'administració web i gestionar-ho tot (usuaris, grups, escriptoris, plantilles, mitjans) dins de la seva categoria.
- **administració**: És capaç d'administrar recursos d'escriptori, baixades, hipervisors i totes les capacitats del sistema disponibles.

Aquí hi ha una vista de taula més granular de les capacitats permeses per elements i accions:

## Capacitats

| Element | Acció | Rol d'usuari | rol avançat | Rol gestor | Rol administrador |
| - | - | - | - | - | - |
| Escriptori | crear        | :material-check: | :material-check: | :material-check: | :material-check: |
| Escriptori | editar          | :material-check: | :material-check: | :material-check: | :material-check: |
| Escriptori | esborrar        | :material-check: | :material-check: | :material-check: | :material-check: |
| Escriptori | visor directe | :material-close: | :material-check: | :material-check: | :material-check: |
| | | | | | |
| Plantilla | crear        | :material-close: | :material-check: | :material-check: | :material-check: |
| Plantilla | editar          | :material-close: | :material-check: | :material-check: | :material-check: |
| Plantilla | esborrar        | :material-close: | :material-close: | :material-check: | :material-check: |
| | | | | | |
| Desplegament | crear        | :material-close: | :material-check: | :material-check: | :material-check: |
| Desplegament | editar          | :material-close: | :material-check: | :material-check: | :material-check: |
| Desplegament | esborrar        | :material-close: | :material-check: | :material-check: | :material-check: |
| Desplegament | videowall     | :material-close: | :material-check: | :material-check: | :material-check: |

## Capacitats en elements no propis

Els usuaris de *rol avançat* a *rol administrador* també poden tenir algunes accions disponibles en els elements que no són propietat seva, o per crear elements a altres usuaris.

Llegenda:

- **desplegament**: El rol pot fer l'acció en els escriptoris que pertanyen als seus desplegaments d'escriptoris virtuals a altres usuaris.
- **categoria**: El rol pot fer l'acció només en els elements que pertanyen a la seva categoria.

| Element | Acció | Rol d'usuari | rol avançat | Rol gestor | Rol administrador |
| | | | | | |
| Escriptori | editar          | :material-close: | despliegue | categoria | :material-check: |
| Escriptori | esborrar        | :material-close: | despliegue | categoria | :material-check: |
| Escriptori | visor directe | :material-close: | despliegue | categoria | :material-check: |
| | | | | | |
| Plantilla | editar          | :material-close: | :material-close: | categoria | :material-check: |
| Plantilla | esborrar        | :material-close: | :material-close: | categoria | :material-check: |
| | | | | | |
| Desplegament | editar          | :material-close: | :material-close: | :material-close: | :material-close: |
| Desplegament | esborrar        | :material-close: | :material-close: | categoria | :material-check: |
| Desplegament | videowall     | :material-close: | :material-close: | :material-close: | :material-close: |