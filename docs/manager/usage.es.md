# Monitorización

!!! warning "Roles con acceso"
    Sólo los **gestores (managers)** tienen acceso a esta característica. Los gestores estan restringidos a su propia categoría.

Este manual ha sido diseñado para proporcionar a los usuarios **gestores** una visión completa sobre cómo supervisar y ajustar de forma efectiva el uso de recursos en la plataforma IsardVDI.

En los próximos apartados, exploraremos cómo utilizar las herramientas y características de monitorización disponibles para obtener información detallada sobre cómo se están utilizando los recursos en vuestro entorno. Desde el seguimiento del consumo de CPU y memoria hasta la gestión de horas consumidas por escritorio, este manual proporcionará los conocimientos necesarios para tomar decisiones informadas y optimizar vuestra configuración de IsardVDI.

Nuestra meta es proporcionar las herramientas y los conocimientos necesarios para maximizar la eficiencia y el rendimiento de vuestro entorno de escritorios virtuales.

Para monitorizar el uso de la plataforma se debe ir al panel de Administración, se pulsa el botón ![](./manage_user.es.images/manage_user1.png)

![](./manage_user.es.images/manage_user2.png){width="90%"}

Se pulsa el botón ![](./usage.images/usage1.png)

![](./usage.images/usage2.png){width="40%"}

## El uso de la plataforma

!!! Info "Conceptos clave"
    * **Item type**: Son los distintos tipos de elementos que pueden consumir recursos dentro del sistema, pueden ser "desktop" (escritorio), "media" (medio), "storage" (almacenamiento) y "user" (usuario).

    * **Parameter**: Cada uno de los distintos consumos registrados. Por ejemplo, cuántos escritorios han arrancado, cuántas horas han sido arrancados, cuánta memoria han utilizado, cuánto almacenamiento se ha utilizado, etc.

La vista principal del apartado de monitorización de uso de la plataforma ofrece información relativa al consumo en un período determinado de tiempo. La columna **end** indica con una flecha cómo ha evolucionado (si ha aumentado o disminuido) este valor en el tiempo escogido. Se mostrará el consumo **acumulativo**, es decir, el **consumo realizado desde el inicio de la utilización de la plataforma**, por tanto, siempre será creciente.

![](./usage.images/usage3.png)

!!! Tip "Sugerencia"
    En vez de visualizar el consumo desde el inicio de la utilización de la plataforma, se puede saber concretamente en cuanto a aumentado/disminuido cada valor en el intervalo escogido. Se puede conseguir de dos formas:

    * Pasando el ratón por encima de la flecha en la columna **end**:

    ![](./usage.images/usage6.png)

    * Seleccionando la casilla ![](./usage.images/usage4.png):

    ![](./usage.images/usage5.png)

Se dispone de varios filtros para ajustar la información a las necesidades del usuario:

![](./usage.images/usage7.png)

* **Dates between**: Indica el período de tiempo a visualizar.
* **Grouping**: Define la agrupación de parámetros a visualizar.

!!! Warning "Atención"
    * El **grouping** depende del item type:

    ![](./usage.images/usage8.png)
* **Consumer**: Dependiendo del tipo de elemento escogido se puede desglosar el consumo de distintas formas. Es decir:
    * De un **escritorio** podemos ver el consumo por categoría, grupo, despliegue, plantilla, usuario e individualmente por cada escritorio.
    * De los **medios**, **almacenamiento** o **usuarios** podemos ver el consumo por categoría, grupo e individualmente por usuario.
* **Group**: Indica concretamente el elemento a visualizar. En caso de no tener ningún valor escogido, muestra la información de todos los elementos disponibles.

![](./usage.images/usage9.png)


## Generar gráficas

Se pueden seleccionar varios elementos marcando las casillas situadas a la izquierda y visualizar gráficamente su consumo pulsando el botón ![](./usage.images/usage10.png)

![](./usage.images/usage11.png)

Abrirá una ventana con una gráfica donde se visualiza la evolución de los distintos parámetros en el intervalo de tiempo escogido. Además, debajo de la gráfica se dispone de una tabla con la información desglosada día a día. Se mostrará el consumo **acumulativo**, es decir, el **consumo realizado desde el inicio de la utilización de la plataforma**, por tanto, la gráfica siempre será creciente:

![](./usage.images/usage12.png)

!!! Warning "Atención"
    * El **grouping** seleccionado puede tener asociadas ciertas limitaciones, éstas se mostrarán al dibujar su gráfica:

    ![](./usage.images/usage20.png)

    Tipo de limitaciones asociadas:

    * **Expected use area**: Zona calculada de consumo medio. Es decir, el uso esperado de la plataforma.
    * **Soft limit**: Al llegar a esta limitación se informará al usuario para que reduzca el consumo de la plataforma.
    * **Hard limit**: Al llegar a esa limitación se bloqueará el uso del sistema.

Se puede saber concretamente en cuanto a aumentado/disminuido cada parámetro de forma diaria pulsando en la casilla ![](./usage.images/usage13.png)

![](./usage.images/usage14.png)

### Visualización de valores

Se puede visualizar los valores de los diferentes parámetros en un punto concreto en la gráfica pasando el ratón por encima:

![](./usage.images/usage15.png)

Se pueden seleccionar/deseleccionar los parámetros a visualizar en la vista de gráfica clicando en cada uno de los nombres localizados en la parte superior de la gráfica:

![](./usage.images/usage16.png)

### Tipo de vista

Se puede cambiar la vista de gráfica **lineal** a gráfica de **barras** haciendo clic en los iconos correspondientes:

![](./usage.images/usage17.png)

### Descarga

#### Imagen

Se puede descargar la gráfica generada como imagen:

![](./usage.images/usage18.png)

#### Tabla

Se puede descargar la tabla con la información desglosada día a día:

![](./usage.images/usage19.png)


