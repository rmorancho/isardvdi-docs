# Debian Desktop 12 install

## Instal·lació del SO

![](installation.images/44.png){width="45%"}
![](installation.images/66.png){width="45%"}


## Isard (rols admin o manager)

**Modificar XML de l'escriptori**

![](../../ubuntu_22.04/desktop/installation/images/modify_xml.png){width="40%"}

```
<driver name="qemu" type="qcow2" cache="unsafe" discard="unmap"/>
```


## Configuració

**Inici de sessió - GNOME Xorg**

![](installation.images/ggnome_xorg_big.png){width="70%"}


### Terminal

**Comandaments bàsic**
```
$ su -
$ visudo 
#add this at the end of file
isard   ALL=(ALL:ALL)   ALL
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim gedit vlc net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```


### Paràmetres

**Privacitat - Pantalla**

![](installation.images/pprivacy-screen.png){width="70%"}

**Compartir - Escriptori remot**

![](installation.images/sharing-remote_desktop.png){width="70%"}

**Contrasenyes i claus - Contrasennyes - Login - Canviar contrasenya**

![](installation.images/passwords.png){width="70%"}

![](installation.images/change_password.png){width="40%"}

**Energia**

![](installation.images/ppower.png){width="70%"}

**Software - Menú dretà - Preferències d'actualitzacions**

![](installation.images/ssoftware-menu-update_preferences.png){width="70%"}


## Personalització
### Terminal

**Canviar el fons de pantalla del login**
```
$ sudo apt install flatpak
$ wget https://dl.flathub.org/repo/appstream/io.github.realmazharhussain.GdmSettings.flatpakref
$ flatpak install ./io.github.realmazharhussain.GdmSettings.flatpakref
$ flatpak run io.github.realmazharhussain.GdmSettings
```

![](installation.images/login_screen_background.png){width="70%"}


### Paràmetres

**Fons de pantalla**

![](installation.images/bbackground.png){width="70%"}

**Usuaris**

![](installation.images/uusers.png){width="70%"}