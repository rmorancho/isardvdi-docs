# Ubuntu Desktop 22.04 Jammy Jellyfish install (GNOME and MATE)

## Instal·lació del SO

![](images/2.png){width="45%"}
![](images/3.png){width="45%"}


## Isard (rols admin o manager)

**Modificar XML de l'escriptori**

![](images/modify_xml.png){width="40%"}

```
<driver name="qemu" type="qcow2" cache="unsafe" discard="unmap"/>
```


## GNOME
### Configuració
#### Terminal

**Comandaments bàsic**
```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim gedit vlc net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```

**Deshabilitar actualitzacions automàtiques i les notificacions de les noves**
```
$ sudo apt purge update-manager update-notifier*
$ sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::DevRelease "false";
$ sudo vim /etc/update-manager/release-upgrades
Prompt=never
$ sudo snap refresh --hold 
$ sudo snap set system refresh.retain=3
```


#### Paràmetres

**Inici de sessió - Ubuntu amb Xorg**

![](images/xorg_session.png){width="70%"}

**Privacitat - Pantalla**

![](images/privacy-screen.png){width="70%"}

**Compartició - Escriptori remot**

![](images/sharing-remote_desktop.png){width="70%"}

Es deixa buida la nova contrasenya que demana, o s'aplica l'**[activació del RDP](../activate_rdp/configuration.ca.md)**.

**Quant a - Actualitzacions de programari**

![](images/software_and_updates.png){width="70%"}


### Personalització
#### Terminal

**Canviar el fons de pantalla del login**

```
$ sudo apt install libglib2.0-dev-bin
$ wget -qO - https://github.com/PRATAP-KUMAR/ubuntu-gdm-set-background/archive/main.tar.gz | tar zx --strip-components=1 ubuntu-gdm-set-background-main/ubuntu-gdm-set-background
$ sudo ./ubuntu-gdm-set-background --image /PATH/TO/YOUR/IMAGE
```


#### Paràmetres

**Fons**

![](images/background.png){width="70%"}

**Usuaris**

![](images/users.png){width="70%"}


## MATE
### Configuració
#### Terminal

**Comandaments bàsics**
```
$ sudo apt update
$ sudo apt upgrade -y
$ sudo apt install vim gedit vlc net-tools htop curl wget spice-vdagent qemu-guest-agent
```

**Modificar *fstab***
```
$ sudo vim /etc/fstab
for every "ext4" storage, define "noatime,discard"

UUID=xxxxx-xxxxx-xxxxxx /               ext4    defaults,noatime,discard,errors=remount-ro 0       1
```

**Alliberar espai al sistema**
```
$ cd /
$ sudo fstrim -a -v 
```

**Reduir la memòria swap**
```
$ sudo sysctl vm.swappiness=1
```

**Reduir emmagatzematge de logs**
```
$ sudo vim /etc/systemd/journald.conf
SystemMaxUse=20M
SystemKeepFree=4G
```

**Deshabilitar les actualitzacions automàtiques i les notificacions de les noves**
```
$ sudo apt purge update-manager update-notifier*
$ sudo vim /etc/apt/apt.conf.d/50unattended-upgrades
Unattended-Upgrade::DevRelease "false";
$ sudo vim /etc/update-manager/release-upgrades
Prompt=never
$ sudo snap refresh --hold 
$ sudo snap set system refresh.retain=3
```

**Habilitar l'auto-login de l'usuari**
```
$ sudo vim /usr/share/lightdm/lightdm.conf.d/50-arctica-greeter.conf
# add next line to the end of file
autologin-user=isard
```

**Habilitar l'accés via RDP**
```
$ sudo apt install xrdp
$ sudo systemctl enable xrdp
$ sudo systemctl restart xrdp

$ sudo vim /etc/X11/Xsession.d/80mate-environment
# add next line before 'fi' closes, last line inside conditional
unset DBUS_SESSION_BUS_ADDRESS
```


#### Paràmetres del sistema

**Administració - Programari i actualitzacions**

![](images/mfglH6f.png){width="70%"}

**Maquinari - Gestor d'energia**

![](images/LM9q2Yz.png){width="70%"}

**Aspecte i comportament - Estalvi de pantalla**

![](images/zH0YWLI.png){width="70%"}


### Personalització
#### Terminal

**Canviar el fons de pantalla del login**
```
$ sudo vim /usr/share/glib-2.0/schemas/30_ubuntu-mate.gschema.override
background='/path/to/wallpaper'
$ sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
```


#### Paràmetres del sistema

**Aspecte i comportament - Aparença - Fons**

![](images/xE9czAP.png){width="70%"}


**Personal - Quant a mi**

![](images/eAwSqAf.png){width="70%"}
