# Casos de uso

Compilación de casos útiles que se pueden adaptar a vuestras necesidades u obtener ideas para construir vuestros propios laboratorios utilizando IsardVDI.

En la sección **Instalaciones de sistemas operativos**, encontraréis guías detalladas para la instalación de los sistemas operativos de escritorios virtuales para Linux y Windows. Estas guías están optimizadas para un entorno de virtualización, asegurando su eficiencia y su correcto funcionamiento.

En la sección **Instalaciones de programas**, encontraréis guías detalladas para la instalación de diferentes programas para los escritorios virtuales.